

translate([0, 0, 0])
{
	import("porta-minis-spray.stl");
	
}

width = 4;

module arm(x, flip = true)
{
	scale([flip ? -1 : 1, 1, 1])
		translate([0, x * 20, 0])
			translate([33, -2, 3])
				difference()
				{
					rotate([0, 45, 0])
						cube(size=[10, width, 10], center=true);
					translate([0, -10, -10])
						cube(size=[10, 20, 20]);	
					rotate([0, 90, 0])
						translate([0, -10, -10])
							cube(size=[10, 20, 20]);
				}
	if (flip)
		arm(x, false);
}

for ( i = [0 : 9])
	arm(i);